var webpack = require('webpack');
var extend = require('extend');
var config = require('./webpack.config.js');
var CleanPlugin = require('clean-webpack-plugin');

var productionConfig = extend(true, {}, config, {
    devtool: 'cheap-module-source-map',
    plugins: [
        new CleanPlugin(['./build'], { verbose: true }),
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('production')
            }
        }),
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.UglifyJsPlugin({
            compressor: {
                warnings: false
            }
        }),
        new webpack.optimize.AggressiveMergingPlugin()]
});


module.exports = productionConfig;
