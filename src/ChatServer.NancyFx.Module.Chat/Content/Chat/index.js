import './src/assests/css/reset.css';
import './src/assests/css/chat.css';
import './src/assests/css/common.css';
import './src/assests/css/login-form.css';

import React from 'react';
import ReactDOM from 'react-dom';
import { browserHistory, Router } from 'react-router';
import { Provider } from 'react-redux';
import { syncHistoryWithStore } from 'react-router-redux';
import configureStore from './src/store/store';
import routes from './src/config/routes';
import injectTapEventPlugin from 'react-tap-event-plugin';

injectTapEventPlugin();

const initialState = window.__initialState__;
const store = configureStore(browserHistory, initialState);
const history = syncHistoryWithStore(browserHistory, store);
const rootElement = document.getElementById('react-app');

ReactDOM.render(
    <Provider store={ store }>
            <Router children={ routes } history={ history } />
    </Provider>,
    rootElement
);
