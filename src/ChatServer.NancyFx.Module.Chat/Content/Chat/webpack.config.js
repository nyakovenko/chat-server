var path = require('path');
var webpack = require('webpack');
var cleanPlugin = require('clean-webpack-plugin');

module.exports = {
    devtool: 'source-map',
    entry: [
        './index.js'
    ],
    output: {
        path: path.resolve(__dirname, './build'),
        filename: 'bundle.js',
        publicPath: '/build/'
    },

    resolve: {
        extensions: ['', '.webpack.js', '.web.js', '.js', '.jsx', '.json']
    },

    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: path.resolve(__dirname, "node_modules"),
                loader: 'babel-loader',
                query: {
                    plugins: ['transform-class-properties'],
                    presets: ['es2015', 'react', 'stage-0']
                }
            },
            {
                test: /\.css?$/,
                loader: "style!css"
            },
            {
                test: /\.(png|jpg|jpeg|gif|svg|woff|woff2)$/,
                loader: "url-loader?limit=10000"
            },
            {
                test: /\.(eot|ttf|wav|mp3)$/,
                loader: 'file-loader'
            }
        ]
    }
};
