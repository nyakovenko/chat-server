import auth from '../auth-service';
import App from '../components/App';
import SignIn from '../components/signin/SignInPage';
import Chat from '../components/Chat';
import React from 'react';
import { browserHistory, Router, Route, Link, withRouter, IndexRoute, IndexRedirect } from 'react-router';

function redirectToIndex(nextState, replace) {
    if (auth.isLoggedIn()) {
        replace('/chat');
    }
}

function requireAuth(nextState, replace){
    if (!auth.isLoggedIn()) {
        replace({
            pathname: '/login',
            state: { nextPathname: nextState.location.pathname }
        });
    }
}


export default (<Router history={browserHistory}>
    <Route path="/" component={App}>
       <IndexRedirect to="/login" />
       <Route path="login" component={SignIn} onEnter={redirectToIndex}/>
       <Route path="chat" component={Chat} onEnter={requireAuth} />
    </Route>
</Router>);