import React from 'react';
import { createStore, compose, applyMiddleware } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import promiseMiddleware from './promise-middleware';
import thunkMiddleware from 'redux-thunk';
import rootReducer from '../reducers';
import auth from '../auth-service';

function getInitialState(){
    return {
        users: {
            data: []
        },
        
        messages: {
            data: []
        },

        auth: {
            signingIn: false,
            signingUp: false,
            loading: false,
            loaded: false,
            user: {
                username: null,
                city: null,
                token: null
            },
            validationErrors: []
        }
    };
}

export default function configureStore(history, initialState) {

    initialState = initialState || getInitialState();

    return createStore(
        rootReducer,
        initialState,
        applyMiddleware(
            thunkMiddleware,
            promiseMiddleware,
            routerMiddleware(history)
        )
    );
}
