import React, {Component, PropTypes} from 'react';

class UserInfo extends Component {

    static propTypes = {
        userName: React.PropTypes.string.isRequired,
        timeMark: React.PropTypes.string.isRequired,
        messageContent: React.PropTypes.string.isRequired,
        IsPersonalMessage: React.PropTypes.bool.isRequired
    };

    render() {
        const { userName, timeMark, messageContent, IsPersonalMessage } = this.props;
        const messageStyle = IsPersonalMessage ? "i" : "not-your-message";

        return (
            <li className={ messageStyle }>
                <div className="head">
                    <span className="name">{ userName }</span>
                    <span className="time">{ timeMark }</span>
                </div>
                <div className="message">{ messageContent }</div>
            </li>
        )
    }
}

export default UserInfo;
