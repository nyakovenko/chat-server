import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import auth from '../auth-service';
import * as authActions from '../actions/authActions';

class App extends Component {

    componentDidMount() {
        const { dispatch } = this.props;
        auth.init();
        dispatch(authActions.loadAuth());
    }

    render() {

        return (
            <div className="app-container">
                { this.props.children }
            </div>
        )
    }
}

export default connect()(App);