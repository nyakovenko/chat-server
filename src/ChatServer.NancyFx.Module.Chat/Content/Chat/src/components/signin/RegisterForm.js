import React, { Component, PropTypes } from 'react';
import * as authActions from '../../actions/authActions';

class RegisterForm extends Component {

    static propTypes = {
        userValidationErrors: PropTypes.array.isRequired,
        dispatch: PropTypes.func.isRequired,
        signingUp: PropTypes.bool.isRequired
    };

    state = {
        username: '',
        password: '',
        confirmPassword: ''
    };

    doPasswordsMatch(){
        if (this.state.confirmPassword.length > 0 && this.state.password.length > 0) {
            if (this.state.password === this.state.confirmPassword) {
                return true;
            }
            return false;
        }
    }

    isSubmitAllowed() {
        if (this.props.signingUp) {
            return false;
        }
        return this.props.userValidationErrors.length === 0 && this.doPasswordsMatch();
    }

    handleUsernameChange(e) {
        this.setState({ username: e.target.value });
    }

    handleCityChange(e) {
        this.setState({ city: e.target.value });
    }

    handlePasswordChange(e) {
        this.setState({ password: e.target.value });
    }

    handlePasswordConfirmationChange(e) {
        this.setState({ confirmPassword: e.target.value });
    }

    submit(e) {
        e.preventDefault();

        const { dispatch } = this.props;
        
        if (!this.state.username.length) {
            this.refs.usernameInput.getInputDOMNode().focus();
        }
        
        if (!this.state.city.length) {
            this.refs.cityInput.getInputDOMNode().focus();
        }

        if (this.state.username.length && !this.state.password.length) {
            this.refs.passwordInput.getInputDOMNode().focus();
        }
        
        if (this.state.username.length && this.state.password.length && !this.state.confirmPassword.length) {
            this.refs.confirmPasswordInput.getInputDOMNode().focus();
        }
        
        if (this.state.username.length && this.state.password.length && this.state.confirmPassword.length) {

            const newUser = {
                username: this.state.username,
                password: btoa(this.state.password),
                city: this.state.city
            };

            dispatch(authActions.register(newUser));
        }
    }

    render()
    {   

        return (
            <form onSubmit={this.submit.bind(this)}>
                <div className="input-container">
                    <input type="text"
                           ref="usernameInput"
                           required="required"
                           value={this.state.username}
                           onChange={this.handleUsernameChange.bind(this)}/>
                    <label for="Username">Username</label>
                    <div className="bar"></div>
                </div>

                <div className="input-container">
                    <input type="text"
                           ref="cityInput"
                           required="required"
                           value={this.state.city}
                           onChange={this.handleCityChange.bind(this)}/>
                    <label for="City">City</label>
                    <div className="bar"></div>
                </div>

                <div className="input-container">
                    <input type="password"
                           ref="passwordInput"
                           required="required"
                           value={this.state.password}
                           onChange={this.handlePasswordChange.bind(this)}/>
                    <label for="Password">Password</label>
                    <div className="bar"></div>
                </div>

                <div className="input-container">
                    <input type="password"
                           ref="confirmPasswordInput"
                           help={this.doPasswordsMatch.bind(this)() === false && 'Your password doesn\'t match'}
                           required="required"
                           value={this.state.confirmPassword}
                           onChange={this.handlePasswordConfirmationChange.bind(this)}/>
                    <label for="Repeat Password">Confirm your password</label>
                    <div className="bar"></div>
                </div>

                <div className="button-container" disabled={ this.isSubmitAllowed.bind(this)() }>
                    <button><span>Next</span></button>
                </div>
            </form>
        )
    }
}

export default RegisterForm;