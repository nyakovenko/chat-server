import React, { Component, PropTypes } from 'react';
import * as authActions from '../../actions/authActions';

class LoginForm extends Component {

    static propTypes = {
        dispatch: PropTypes.func.isRequired,
        isLoggedIn: PropTypes.bool.isRequired,
        signingIn: PropTypes.bool.isRequired
    };

    state = {
        username: '',
        password: ''
    };

    componentDidMount() {
        if (this.state.username.length) {
            this.refs.passwordInput.focus();
        } else {
            this.refs.usernameInput.focus();
        }
    }

    isSubmitAllowed() {
        if (this.props.signingIn) {
            return false;
        }
        return true;
    }

    handleUsernameChange(e) {
        this.setState({ username: e.target.value });
    }

    handlePasswordChange(e) {
        this.setState({ password: e.target.value });
    }

    submit(e){
        e.preventDefault();
        const { dispatch } = this.props;

        if (this.state.username.length < 1) {
            this.refs.usernameInput.focus();
        }

        if (this.state.username.length > 0 && this.state.password.length < 1) {
            this.refs.passwordInput.focus();
        }

        if (this.state.username.length > 0 && this.state.password.length > 0) {
            var user = {
                username: this.state.username,
                password: btoa(this.state.password)
            };
            dispatch(authActions.login(user));
        }
    }

    render() {

        //let { isLoggedIn } = this.props;

        //if (isLoggedIn) {
        //    this.setState({ username: '', password: ''});
        //}

        return (
            <form onSubmit={ this.submit.bind(this) }>

                    <div className="input-container">
                        <input
                            ref="usernameInput"
                            type="text"
                            required="required"
                            value={this.state.username}
                            onChange={this.handleUsernameChange.bind(this)}/>
                        <label for="Username">Username</label>
                        <div className="bar"></div>
                    </div>

                    <div className="input-container">
                        <input
                            ref="passwordInput"
                            type="password"
                            required="required"
                            value={this.state.password}
                            onChange={this.handlePasswordChange.bind(this)}/>
                        <label for="Password">Password</label>
                        <div className="bar"></div>
                    </div>

                    <div className="button-container" disabled={ this.isSubmitAllowed.bind(this)() }>
                        <button><span>Go</span></button>
                    </div>

                </form>
        )
    }
}

export default LoginForm;