import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import LoginForm from './LoginForm';
import RegisterForm from './RegisterForm';

class SignInPage extends Component {
    state = {
        isRegisterFormVisible: false
    };

    switchToAlternateForm(e){
        e.preventDefault();

        let isRegisterFormVisible = this.state.isRegisterFormVisible;
        let container = $(this.refs.signInContainer);

        if(this.state.isRegisterFormVisible) {
            container.stop().removeClass('active');
        }else{
            container.stop().addClass('active');
        }

        this.setState({ isRegisterFormVisible: !isRegisterFormVisible });
    }
    
    render() {

        return (
            <div className="container" ref="signInContainer">
                <div className="card"></div>

                <div className="card">
                    <h1 className="title">Login</h1>
                    <LoginForm {...this.props}/>
                </div>

                <div className="card alt">
                    <div className="toggle" onClick={ this.switchToAlternateForm.bind(this) }></div>

                    <h1 className="title">Register
                        <div className="close" onClick={ this.switchToAlternateForm.bind(this) }></div>
                    </h1>

                    <RegisterForm {...this.props}/>
                </div>
            </div>
        )
    }
}


function mapStateToProps(state) {
    return {
        userValidationErrors: state.auth.validationErrors,
        isLoggedIn: state.auth.loaded,
        signingIn: state.auth.signingIn,
        signingUp: state.auth.signingUp,
        user: state.auth.user
    }
}

export default connect(mapStateToProps)(SignInPage);
