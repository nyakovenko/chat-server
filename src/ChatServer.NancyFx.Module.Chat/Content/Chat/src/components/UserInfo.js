import React, {Component, PropTypes} from 'react';

class UserInfo extends Component {

    static propTypes = {
        userName: React.PropTypes.string.isRequired
    };

    render() {
        const { userName } = this.props;

        return (
            <li>
                <i className="user-info-icon"/>
                <div className="info">
                    <div className="user">{ userName }</div>
                    <div className="status"></div>
                </div>
            </li>
        )
    }
}

export default UserInfo;
