import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import UserList from "./UserList";
import Message from "./Message";
import auth from '../auth-service';
import * as messageActions from '../actions/messageActions';
import * as usersActions from '../actions/usersActions';

class Chat extends Component {

    static propTypes = {
        users: PropTypes.array.isRequired,
        messages: PropTypes.array.isRequired,
        dispatch: PropTypes.func.isRequired,
        userInfo: PropTypes.object.isRequired
    };

    state = {
        isConnected: false
    };

    constructor(props, context) {
        super(props, context);
        this.handleKeyPress = this.handleKeyPress.bind(this);
    }

    componentDidMount(){
        var self = this,
            token = auth.getToken(),
            { dispatch } = this.props;
            self.unsendMessages = [];

        this.refs.inputText.addEventListener("keypress", this.handleKeyPress, false);

        this.chatHub = $.connection.chatHub;

        $.connection.hub.qs = { 'token': token };
        $.connection.hub.logging = true;
        $.connection.hub.error(function(error) {
            console.log('SignalR error: ' + error);
        });

        $.connection.hub.reconnected(function() {
            self.unsendMessages.forEach(msg => {
                self.sendMessage(msg);
            });
        });

        this.chatHub.client.broadcastMessage = function(content, timeMark, username) {
            var message = self.composeMessage(content, username, timeMark, false);
            dispatch(messageActions.sendMessage(message));
        };

        this.chatHub.client.clientConnected = function(client) {
            dispatch(usersActions.clientConnected({ userName: client }));
        };
        
        this.chatHub.client.receiveListOfClients = function(clients) {
            dispatch(usersActions.listOfClients(clients || []));
        };

        this.chatHub.client.clientDisconnected = function(client) {
            dispatch(usersActions.clientDisconnected({ userName: client }));
        };

        $.connection.hub.start({ 'token': token })
            .done(function() {
                self.setState({ isConnected: true });
                let client = self.props.userInfo.user.username;
                dispatch(usersActions.clientConnected({ userName: client}));
            })
            .fail(function() {
                self.setState({ isConnected: false });
            });
    }

    componentWillUnmount() {
        this.refs.inputText.removeEventListener("keypress", this.handleKeyPress, false);
        $.connection.hub.stop();
    }

    handleKeyPress = (e) => {
        if (e.keyCode === 13) {
            this.sendMessage();
            return false;
        }
    }

    composeMessage(content, userName, timeMark, IsPersonal) {
        return {
            Content: content, 
            UserName: userName,
            TimeMark: timeMark, 
            IsPersonal: IsPersonal
        };
    }

    sendMessage(message) {
        let { dispatch, userInfo } = this.props;
        let textNode = this.refs.inputText;
        let self = this;

        if (!message) {
            if (textNode.value.length > 0) {
                let currentTime = new Date().toTimeString().split(' ')[0];
                message = this.composeMessage(textNode.value, userInfo.user.username, currentTime , true);
            } else {
                return;
            }
        }

        dispatch(messageActions.sendMessage(message));
        textNode.value = '';

        this.chatHub.server.sendMessage(message.Content, message.TimeMark)
            .fail(function(error) {
                self.unsendMessages.push(message);
            });
    };

    render() {
        const { users, messages } = this.props;

        var messageComponents = messages.map((message, index) => {
            return <Message key={ index }
                userName={ message.UserName }
                timeMark={ message.TimeMark }
                messageContent={ message.Content }
                IsPersonalMessage={ message.IsPersonal }
            />
        });

        return (
            <div className="ui">

                <div className="left-menu">
                    <UserList users={ users }/>
                </div>

                <div className="chat">
                    <ul className="messages">
                        { messageComponents }
                    </ul>
                    
                    <div className="write-form">
                        <textarea placeholder="Type your message" ref="inputText"  rows="2"/>
                        <span className="send" onClick={ this.sendMessage.bind(this) }>Send</span>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        users: state.users.data,
        messages: state.messages.data,
        userInfo: state.auth
    }
}
export default connect(mapStateToProps)(Chat);