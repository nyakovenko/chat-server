import React, { Component, PropTypes } from 'react';
import User from './UserInfo';

class UserList extends Component {

    static propTypes = {
        users: React.PropTypes.array
    };

    render() {

        var users = this.props.users || [];

        var userComponents = users.map((user, index) => {
            return <User key={ index } userName={ user.userName } />
        });

        return (
            <menu className="list-friends">
                { userComponents }
            </menu>
        )
    }
}

export default UserList;