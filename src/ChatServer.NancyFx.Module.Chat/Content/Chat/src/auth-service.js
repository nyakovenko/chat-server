const TOKEN_KEY = 'token';

const authService = {
    init: function() {
        var token = this.getToken();

        if (!token) {
            return;
        }

        $.ajax({
                url: '/status',
                method: 'get',
                async: false,
                headers: { 'Accept': 'application/json', 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token }
            }).done((response, statusText, xhr) => {
                if (xhr.status === 401 || xhr.status === 401) {
                    localStorage.removeItem(TOKEN_KEY);
                }
            })
            .fail((jqXHR, textStatus, errorThrown) => {
                localStorage.removeItem(TOKEN_KEY);
            });
    },
    isLoggedIn: function() {
        return this.getToken() != null;
    },
    setToken: function(token) {
        localStorage.setItem(TOKEN_KEY, token);
    },
    getToken: function() {
        return localStorage.getItem(TOKEN_KEY);
    },
    removeToken: function() {
        localStorage.removeItem(TOKEN_KEY);
    }
};

export default authService;