import {
    AUTH_LOAD,
    AUTH_LOAD_SUCCESS,
    AUTH_LOAD_FAIL,
    AUTH_LOGIN,
    AUTH_LOGIN_SUCCESS,
    AUTH_LOGIN_FAIL,
    AUTH_LOGOUT,
    AUTH_LOGOUT_SUCCESS,
    AUTH_LOGOUT_FAIL,
    AUTH_REGISTRATION,
    AUTH_REGISTRATION_SUCCESS,
    AUTH_REGISTRATION_FAIL,
} from '../constants/authActionTypes';

const initialState = {
    loading: false,
    signingIn: false,
    signingUp: false,
    loaded: false,
    user: {
        city: null,
        username: null,
        token: null
    }
};

export default function auth(state = initialState, action = {}) {
    switch (action.type) {

        case AUTH_LOAD:
            return {
                ...state,
                loading: true
            };
        case AUTH_LOAD_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                user: { ...state.user, username: action.username, city: action.city }
            };
        case AUTH_LOAD_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                user: {
                    city: null,
                    username: null,
                    token: null
                }
            };

        case AUTH_LOGIN:
            return {
                ...state,
                signingIn: true
            };
        case AUTH_LOGIN_SUCCESS:
            return {
                ...state,
                signingIn: false,
                loaded: true,
                user: {
                    username: action.user.userName,
                    city: action.user.city,
                    token: action.token
                }
            };
        case AUTH_LOGIN_FAIL:
            return {
                ...state,
                signingIn: false,
                user: {
                    username: null,
                    city: null,
                    token: null
                },
                signInError: action.error
            };

        case AUTH_REGISTRATION:
            return {
                ...state,
                signingUp: true
            };
        case AUTH_REGISTRATION_SUCCESS:
            return {
                ...state,
                signingUp: false
            };
        case AUTH_REGISTRATION_FAIL:
            return {
                ...state,
                loaded: false,
                signingUp: false,
                user: {
                    username: null,
                    token: null,
                    city: null
                }
            };

        //case AUTH_LOGOUT:
        //    return {
        //        ...state,
        //        signingOut: true
        //    };
        //case AUTH_LOGOUT_SUCCESS:
        //    return {
        //        ...state,
        //        signingOut: false,
        //        loaded: false,
        //        user: {
        //            username: null,
        //            id: null
        //        }
        //    };
        //case AUTH_LOGOUT_FAIL:
        //    return {
        //        ...state,
        //        signingOut: false,
        //        signOutError: action.error
        //    };

        default:
            return state;
    }
}
