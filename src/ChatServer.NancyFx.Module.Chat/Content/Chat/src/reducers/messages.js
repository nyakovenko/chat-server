import {
    MESSAGE_SEND
} from '../constants/messagesActionTypes';

const initialState = {
    data: []
};

export default function messages(state = initialState, action) {
    switch (action.type) {
        case MESSAGE_SEND:
            return {...state,
                data: [...state.data, action.message]
            };
        default:
            return state;
    }
}