import auth from './auth';
import messages from './messages';
import users from './users';
import {combineReducers} from 'redux';
import { routerReducer as routing } from 'react-router-redux';

const rootReducer = combineReducers({
    auth,
    messages,
    users,
    routing
});

export default rootReducer;
