import {
    CLIENT_CONNECTED,
    CLIENT_DISCONNECTED,
    CLIENT_LIST
} from '../constants/usersActionTypes';


const initialState = {
    data: []
};

export default function users(state = initialState, action) {
    switch (action.type) {
        case CLIENT_CONNECTED:
            return {
                ...state,
                data: [...state.data, action.client]
        };
        case CLIENT_LIST:
            return {
                ...state,
                data: [...state.data, ...action.clients]
            };
        case CLIENT_DISCONNECTED:   
            var target = $.grep(state.data, function(e){ return e.userName === action.client.userName; });
            state.data.remove(target);
            return {
                ...state,
                data: [...state.data]
            };
        default:
            return state;
    }
}