import * as types from '../constants/authActionTypes';
import authService from '../auth-service';
import { browserHistory } from 'react-router';


export function loadAuth() {
    return dispatch => {
        
        if (!authService.isLoggedIn) {
            dispatch(loadAuthFailed());
            return browserHistory.push('/login');
        }

        const authFailed = function() {
            dispatch(loadAuthFailed());
            authService.removeToken();
            browserHistory.push('/login');
        }
        return $.ajax({
                url: '/status',
                method: 'get',
                headers: { 'Accept': 'application/json', 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + authService.getToken() }
        }).done((response, statusText, xhr) => {
            if (xhr.status === 200) {
                console.log(response);
                    dispatch(loadAuthSucceed(response.userName, response.city));
                    browserHistory.push('/chat');
                }
                    if (xhr.status === 401 || xhr.status === 401) {
                    authFailed();
                }
            })
            .fail((jqXHR, textStatus, errorThrown) => {
                authFailed();
            });
    };
    
}

function loadAuthFailed() {
    return {
        type: types.AUTH_LOAD_FAIL
    };
}

function loadAuthSucceed(username, city) {
    return {
        type: types.AUTH_LOAD_SUCCESS,
        username,
        city
    };
}
/*
* LOGIN ACTIONS
* */
function requestLogin() {
    return {
        type: types.AUTH_LOGIN
    }
}

function loginSucceeded(user, token) {
    return {
        type: types.AUTH_LOGIN_SUCCESS,
        user,
        token
    };
}

function loginFailed(error) {
    return {
        type: types.AUTH_LOGIN_FAIL,
        error
    }
}

export function login(user) {
    return dispatch => {

        dispatch(requestLogin());

        return $.ajax({
                url: 'api/rest/token',
                method: 'post',
                headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
                data: JSON.stringify(user)
            })
            .done((response, statusText, xhr) => {
                console.log(response);
                console.log(user);
                if (xhr.status === 200) {
                    authService.setToken(response.accessToken);
                    dispatch(loginSucceeded(user, response.accessToken));
                    browserHistory.push('/chat');
                } else {
                    dispatch(loginFailed(statusText));
                }
            })
            .fail((jqXHR, statusText, errorThrown) => {
                dispatch(loginFailed(statusText));
            });
    };
}

/*
* REGISTRATION ACTIONS
* */
function requestRegistration() {
    return {
        type: types.AUTH_REGISTRATION
    }
}

function registrationSucceeded(username) {
    const newUser = {
        name: username,
        id: Symbol(username)
    };

    return {
        type: types.AUTH_REGISTRATION_SUCCESS,
        newUser
    }
}

function registrationFailed(error) {
    return {
        type: types.AUTH_REGISTRATION_FAIL,
        error
    }
}

export function register(newUser) {
    return dispatch => {

        dispatch(requestRegistration());

        return $.ajax({
                url: 'api/rest/users/register',
                method: 'post',
                headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
                data: JSON.stringify(newUser)
            })
            .done((response, statusText, xhr) => {
                console.log(response);
                if (xhr.status === 200) {
                    dispatch(login(newUser));
                } else {
                    dispatch(registrationFailed(statusText));
                }
            })
            .fail((jqXHR, statusText, errorThrown) => {
                dispatch(registrationFailed(statusText));
            });
    };
}