import * as types from '../constants/messagesActionTypes';

export function sendMessage(message) {
    return {
        type: types.MESSAGE_SEND,
        message
    }
}