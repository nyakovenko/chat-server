import * as types from '../constants/usersActionTypes';

export function clientConnected(client) {
    return {
        type: types.CLIENT_CONNECTED,
            client
    }
}

export function clientDisconnected(client) {
    return {
        type: types.CLIENT_DISCONNECTED,
            client
        }
}

export function listOfClients(clients) {
    return {
        type: types.CLIENT_LIST,
        clients
        }
}