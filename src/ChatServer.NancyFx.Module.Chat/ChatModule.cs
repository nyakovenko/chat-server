﻿using System;
using ChatServer.NancyFx.Authentication;
using Nancy;
using Nancy.Security;

namespace ChatServer.NancyFx.Module.Chat
{

    public class ChatModule : NancyModule
    {
        public ChatModule()
        {
            Get["/"] = _ =>
            {
                return
                    Negotiate.WithView("Chat/index")
                        .WithModel(new { Token = Guid.NewGuid().ToString() })
                        .WithStatusCode(HttpStatusCode.OK);
            };

            Get["/login"] = _ =>
            {
                return Response.AsRedirect("/");
            };

            Get["/chat"] = _ =>
            {
                return Response.AsRedirect("/");
            };

            Get["/status"] = _ =>
            {
                this.RequiresAuthentication();
                var userInfo = this.Context.CurrentUser as ExtendedUserIdentity;
                return Negotiate.WithModel(userInfo).WithStatusCode(HttpStatusCode.OK);
            };
        }
    }
}
