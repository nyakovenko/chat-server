﻿using System.Collections.Generic;
using Nancy.Security;

namespace ChatServer.NancyFx.Authentication
{
    public class ExtendedUserIdentity : IUserIdentity
    {
        public string UserName { get; set; }
        public IEnumerable<string> Claims { get; set; }
        public string City { get; set; }
    }
}
