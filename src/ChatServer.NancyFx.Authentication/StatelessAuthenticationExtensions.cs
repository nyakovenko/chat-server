﻿using System.Collections.Generic;
using ChatServer.Contracts.Models;
using Nancy.Security;

namespace ChatServer.NancyFx.Authentication
{
    public static class StatelessAuthenticationExtensions
    {
        public static IUserIdentity ToNancyUserIdentity(this IdentityToken token)
        {
            return new ExtendedUserIdentity()
            {
                City = token.City,
                UserName = token.UserName,
                Claims = new List<string>(token.Claims)
            };
        }
    }
}
