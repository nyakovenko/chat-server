﻿using System;
using ChatServer.Contracts;
using ChatServer.Contracts.Auth;
using ChatServer.Contracts.Models;
using ChatServer.NancyFx.Misc;
using JWT;
using Nancy;
using NLog;

namespace ChatServer.NancyFx.Authentication
{
    public class NancyUserIdentityProvider : IUserIdentityProvider
    {
        private readonly string _secret;
        private readonly ITokenProvider _tokenProvider;
        private static readonly ILogger Logger = LogManager.GetCurrentClassLogger();

        public NancyUserIdentityProvider(IServerConfiguration configuration, ITokenProvider tokenProvider)
        {
            _tokenProvider = tokenProvider;
            this._secret = configuration.Crypto.Secret;
        }

        public IdentityToken GetUserIdentity(object ctx)
        {
            var token = this._tokenProvider.RequestToken(ctx);

            if (token == null)
            {
                return null;
            }

            try
            {
                return JsonWebToken.DecodeToObject<IdentityToken>(token, this._secret, verify: true);
            }
            catch (SignatureVerificationException ex)
            {
                Logger.Debug(ex.Message);
                ((NancyContext)ctx).Response = JsonNetResponse.Create(new { Reason = ex.Message },
                    HttpStatusCode.Unauthorized);
                return null;
            }
            catch (Exception ex)
            {
                Logger.Fatal(ex, "There is fatal error occured during Token body decryption. Please check token body. Request terminated.");
                return null;
            }
        }
    }
}
