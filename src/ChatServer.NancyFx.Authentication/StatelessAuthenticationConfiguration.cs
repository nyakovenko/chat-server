﻿using System;
using ChatServer.Contracts.Models;
using Nancy;

namespace ChatServer.NancyFx.Authentication
{
    public class StatelessAuthenticationConfiguration
    {
        internal readonly Func<NancyContext, IdentityToken> GetUserIdentity;

        public StatelessAuthenticationConfiguration(Func<NancyContext, IdentityToken> getUserIdentity)
        {
            GetUserIdentity = getUserIdentity;
        }

        public virtual bool IsValid
        {
            get
            {
                return GetUserIdentity != null;
            }
        }
    }
}
