﻿using System;
using System.Linq;
using ChatServer.Contracts;
using ChatServer.Contracts.Auth;
using ChatServer.Host.Common.Constants;
using ChatServer.NancyFx.Misc;
using Nancy;

namespace ChatServer.NancyFx.Authentication
{
    public class NancyTokenProvider : ITokenProvider
    {
        public string RequestToken(object ctx)
        {
            var context = ctx as NancyContext;

            if (context == null)
            {
                return null;
            }

            var nancyRequest = context.Request;

            var tokenData = nancyRequest.Headers[AuthConsts.AuthorizationHeaderKey].FirstOrDefault();

            if (tokenData == null)
            {
                return null;
            }

            var bearerSplit = tokenData.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);

            if (bearerSplit.Count() < 2)
            {
                context.Response = JsonNetResponse.Create(new { Reason = "Invalid Authorization header format" },
                    HttpStatusCode.Unauthorized);
                return null;
            }

            if (bearerSplit[0] != AuthConsts.BearerToken)
            {
                context.Response = JsonNetResponse.Create(new { Reason = "Invalid token type" },
                    HttpStatusCode.Unauthorized);
                return null;
            }

            var token = bearerSplit[1];

            return token;
        }
    }
}
