﻿using System;
using ChatServer.Host.Common.Constants.Services;
using ChatServer.NancyFx.Misc;
using Nancy;

namespace ChatServer.NancyFx.Authentication
{
    public class StatelessAuthenticationHook
    {
        private const string TokenServicePath =
            "/" + TokenModuleConst.Service.ServiceRootPath + TokenModuleConst.UriTemplate.TokenUri;

        public static Func<NancyContext, Response> HandleRequest(StatelessAuthenticationConfiguration configuration)
        {
            if (configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }

            if (!configuration.IsValid)
            {
                throw new ArgumentException("Configuration is invalid", "configuration");
            }

            return context =>
            {
                if (IsTokenService(context.Request.Path))
                {
                    return context.Response;
                }

                try
                {
                    var identityToken = configuration.GetUserIdentity(context);

                    if (identityToken == null)
                    {
                        return context.Response;
                    }

                    context.CurrentUser = identityToken.ToNancyUserIdentity();
                    return context.Response;
                }
                catch (Exception ex)
                {
                    return JsonNetResponse.Create(new { Reason = ex.Message }, HttpStatusCode.Unauthorized);
                }
            };
        }

        private static bool IsTokenService(string path)
        {
            return (path == TokenServicePath);
        }
    }
}
