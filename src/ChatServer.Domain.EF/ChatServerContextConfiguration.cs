﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.SqlServer;

namespace ChatServer.Domain.EF
{
    class ChatServerContextConfiguration : DbConfiguration
    {
        public ChatServerContextConfiguration()
        {
            this.SetProviderServices("System.Data.SqlClient", SqlProviderServices.Instance);
            this.SetDefaultConnectionFactory(new SqlConnectionFactory()); 
        }
    }
}
