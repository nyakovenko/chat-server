﻿using System.Data.Entity;
using ChatServer.Common.Cryptography;
using ChatServer.Contracts.Configuration;
using ChatServer.Domain.Contracts.Models;

namespace ChatServer.Domain.EF
{
    class ChatServerContextInitializer : CreateDatabaseIfNotExists<ChatServerContext>
    {
        private readonly CryptographyConfiguration _crypto;
        public ChatServerContextInitializer(CryptographyConfiguration crypto)
        {
            this._crypto = crypto;
        }

        protected override void Seed(ChatServerContext context)
        {
            string password, salt;
            PasswordGenerator.CreatePassword(
                this._crypto.Secret, this._crypto.SaltLength, "adminletmein", out password, out salt);

            var admin = new User()
            {
                City = "Voronezh",
                Claims = "Admin",
                Username = "admin",
                Password = password,
                Salt = salt
            };

            context.Users.Add(admin);
            context.SaveChanges();
        }
    }
}
