﻿using System;
using System.Data.Entity;
using ChatServer.Domain.Contracts;
using ChatServer.Domain.Contracts.Models;

namespace ChatServer.Domain.EF
{
    public interface IChatServerContext : IUnitOfWork, IDisposable
    {
        DbSet<TEntity> Set<TEntity>() where TEntity : class;

        IDbSet<User> Users { get; set; }
        IDbSet<Message> Messages { get; set; }
    }
}
