﻿using System.Data.Entity.ModelConfiguration;
using ChatServer.Domain.Contracts.Models;

namespace ChatServer.Domain.EF.Configurations
{
    class UserConfiguration : EntityTypeConfiguration<User>
    {
        public UserConfiguration()
        {
            this.Property(e => e.Username).IsRequired().HasMaxLength(255);
            this.Property(e => e.Claims).IsRequired().HasMaxLength(255);
            this.Property(e => e.City).IsRequired().HasMaxLength(255);

            this.Property(e => e.Password).IsRequired();
            this.Property(e => e.Salt).IsRequired();

            this.HasMany(e => e.Messages)
                .WithRequired(e => e.Publisher)
                .WillCascadeOnDelete(false);
        }
    }
}
