﻿using System.Data.Entity.ModelConfiguration;
using ChatServer.Domain.Contracts.Models;

namespace ChatServer.Domain.EF.Configurations
{
    class MessageConfiguration : EntityTypeConfiguration<Message>
    {
        public MessageConfiguration()
        {
            this.Property(e => e.Date)
                   .IsRequired();

            this.Property(e => e.PublisherName)
                .IsRequired()
                .HasMaxLength(255);
        }
    }
}
