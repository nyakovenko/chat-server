﻿using System;
using System.Data.Entity;
using ChatServer.Contracts;
using ChatServer.Domain.Contracts.Models;
using ChatServer.Domain.EF.Configurations;
using NLog;

namespace ChatServer.Domain.EF
{
    public class ChatServerContext : DbContext, IChatServerContext
    {
        public ILogger Logger = LogManager.GetCurrentClassLogger();

        public ChatServerContext(IServerConfiguration config)
            : base(config.DbConnectionString)
        {
            Database.SetInitializer(new ChatServerContextInitializer(config.Crypto));
            DbConfiguration.SetConfiguration(new ChatServerContextConfiguration());

            this.Database.Log = sql =>
            {
                if (Logger.IsDebugEnabled)
                {
                    Logger.Debug("slq generated - {0}", sql.TrimEnd());
                }
            };
        }

        public IDbSet<User> Users { get; set; } 
        public IDbSet<Message> Messages { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Properties<DateTime>()
                .Configure(c => c.HasColumnType("datetime2"));

            modelBuilder.Configurations.Add(new UserConfiguration());
            modelBuilder.Configurations.Add(new MessageConfiguration());
        }
    }
}
