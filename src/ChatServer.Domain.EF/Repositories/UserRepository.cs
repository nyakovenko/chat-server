﻿using System;
using System.Linq;
using System.Threading.Tasks;
using ChatServer.Domain.Contracts;
using ChatServer.Domain.Contracts.Models;

namespace ChatServer.Domain.EF.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(IChatServerContext dbContext)
            : base(dbContext)
        {
        }

        public async Task<User> FindByUserName(string username)
        {
            if (string.IsNullOrEmpty(username))
            {
                throw new ArgumentNullException(username);
            }

            var results = await this.FindBy(user => user.Username == username);
            return results.FirstOrDefault();
        }
    }
}
