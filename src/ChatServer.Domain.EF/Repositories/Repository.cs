﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChatServer.Domain.Contracts;
using ChatServer.Domain.Contracts.Models;

namespace ChatServer.Domain.EF.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        private readonly IChatServerContext _dbContext;

        public Repository(IChatServerContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public async Task<TEntity> Create(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            var results = await this.Create(new List<TEntity>() { entity });

            return results.FirstOrDefault();
        }

        public async Task<IEnumerable<TEntity>> Create(IList<TEntity> entities)
        {
            if (entities == null || !entities.Any())
            {
                throw new ArgumentNullException("entities");
            }

            try
            {
                var results = entities.Select(instance => this._dbContext.Set<TEntity>().Add(instance)).ToList();
                await this._dbContext.SaveChangesAsync();
                return results;
            }
            catch (DbEntityValidationException ex)
            {
                throw new Exception(FormatValidationException(ex), ex);
            }
            
        }

        public async Task<IEnumerable<TEntity>> GetAll()
        {
            return await this._dbContext.Set<TEntity>().ToListAsync();
        }

        public async Task<IEnumerable<TEntity>> FindBy(Func<TEntity, bool> filter)
        {
            return await Task.FromResult(this._dbContext.Set<TEntity>().Where(filter).ToList());
        }

        public async Task<TEntity> Update(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            try
            {
                var instance = this._dbContext.Set<TEntity>().Attach(entity);
                ((DbContext)this._dbContext).Entry(instance).State = EntityState.Modified;
                await this._dbContext.SaveChangesAsync();
                return instance;
            }
            catch (DbEntityValidationException ex)
            {
                throw new Exception(FormatValidationException(ex), ex);
            }
        }

        public async Task Delete(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            try
            {
                this._dbContext.Set<TEntity>().Remove(entity);
                await this._dbContext.SaveChangesAsync();
            }
            catch (DbEntityValidationException ex)
            {
                throw new Exception(FormatValidationException(ex), ex);
            }
        }

        protected virtual string FormatValidationException(DbEntityValidationException ex)
        {
            var sb = new StringBuilder();
            foreach (var validationErrors in ex.EntityValidationErrors)
            {
                foreach (var validationError in validationErrors.ValidationErrors)
                {
                    sb.AppendLine(string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage));
                }
            }
            return sb.ToString();
        }
    }
}
