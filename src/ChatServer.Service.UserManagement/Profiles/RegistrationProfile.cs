﻿using AutoMapper;
using ChatServer.Common.Mapping;
using ChatServer.Contracts.Requests.User;
using ChatServer.Domain.Contracts.Models;

namespace ChatServer.Service.UserManagement.Profiles
{
    public class RegistrationProfile : Profile
    {
        protected override void Configure()
        {
            this.CreateMap<RegisterUserRequest, User>()
                .ForMember(dest => dest.Username, opt => opt.MapFrom(src => src.Username))
                .ForMember(dest => dest.City, opt => opt.MapFrom(src => src.City))
                .IgnoreAllNonExisting();
        }
    }
}
