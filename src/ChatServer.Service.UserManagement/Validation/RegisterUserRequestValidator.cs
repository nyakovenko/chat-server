﻿using ChatServer.Contracts.Requests.User;
using FluentValidation;

namespace ChatServer.Service.UserManagement.Validation
{
    public class RegisterUserRequestValidator : AbstractValidator<RegisterUserRequest>
    {
        public RegisterUserRequestValidator()
        {
            RuleFor(x => x.Username)
                .NotNull()
                .WithMessage("Username required");

            RuleFor(x => x.City)
                .NotEmpty()
                .WithMessage("City required")
                .Must(x => x.Length > 1 && x.Length < 255)
                .WithMessage("City should be between 1 and 255 chars");

            RuleFor(x => x.Password)
                .NotEmpty()
                .WithMessage("Password required");
        }
    }
}
