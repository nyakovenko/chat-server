﻿using System.Threading.Tasks;
using ChatServer.Common.Cryptography;
using ChatServer.Common.Mapping;
using ChatServer.Contracts;
using ChatServer.Contracts.Requests.User;
using ChatServer.Contracts.Responses.User;
using ChatServer.Contracts.Services;
using ChatServer.Domain.Contracts;
using ChatServer.Domain.Contracts.Models;

namespace ChatServer.Service.UserManagement
{
    public class RegistrationService : IRegistrationService
    {
        private readonly IUserRepository _userIdentityRepository;
        private readonly IMapper<RegisterUserRequest, User> _toUserIdentityMapper;
        private readonly string _secretKey;
        private readonly uint _saltSize;

        public RegistrationService(
            IUserRepository userIdentityRepository,
            IServerConfiguration config,
            IMapper<RegisterUserRequest, User> toUserIdentityMapper)
        {
            this._toUserIdentityMapper = toUserIdentityMapper;
            this._userIdentityRepository = userIdentityRepository;
            this._secretKey = config.Crypto.Secret;
            this._saltSize = config.Crypto.SaltLength;
        }

        public virtual async Task<RegisterUserResponse> Register(RegisterUserRequest request)
        {
            var response = new RegisterUserResponse();

            var user = await this._userIdentityRepository.FindByUserName(request.Username);

            if (user != null)
            {
                return response.ToUnableRegisterNewUserResponse(request.Username);
            }

            string passwordHash, salt;
            PasswordGenerator.CreatePassword(this._secretKey, this._saltSize, request.Password, out passwordHash, out salt);

            var newUser = this._toUserIdentityMapper.Map(request);
            newUser.Password = passwordHash;
            newUser.Salt = salt;

            await this._userIdentityRepository.Create(newUser);

            return response.ToSuccessfulResponse(newUser.Username);
        }
    }
}
