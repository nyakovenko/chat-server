﻿using System;
using System.Collections.Generic;
using System.IO;
using ChatServer.Host.Common.Constants;
using ChatServer.Host.Common.Modules;
using ChatServer.Host.Common.Modules.Common;

namespace CharServer.Host.SelfHost.Bootstrap.Modules
{
    public class ValidationModule : CommonValidationModule
    {
        public override IList<string> AssembliesToScan
        {
            get
            {
                var path = AppDomain.CurrentDomain.BaseDirectory;
                var serviceFiles = Directory.GetFiles(path, HostConsts.AssembliesForScanMask);
                var files = new List<string>();
                files.AddRange(serviceFiles);
                return files;
            }
        }
    }
}
