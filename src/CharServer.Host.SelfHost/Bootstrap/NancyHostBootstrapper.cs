﻿using System.Diagnostics;
using Autofac;
using Autofac.Builder;
using CharServer.Host.SelfHost.Bootstrap.Modules;
using ChatServer.Common.Container;
using ChatServer.Contracts;
using ChatServer.Contracts.Auth;
using ChatServer.Host.Common.Modules;
using ChatServer.NancyFx.Authentication;
using ChatServer.NancyFx.Misc.Hooks;
using Nancy;
using Nancy.Bootstrapper;
using Nancy.Bootstrappers.Autofac;
using NLog;

namespace CharServer.Host.SelfHost.Bootstrap
{
    class NancyHostBootstrapper : AutofacNancyBootstrapper
    {
        private readonly ILogger _logger = LogManager.GetCurrentClassLogger();
        private IContainer _container;

        public NancyHostBootstrapper(IContainer container)
        {
            this._container = container;
        }

        protected override ILifetimeScope GetApplicationContainer()
        {
            if (this._container == null)
            {
                this._container = new ContainerBuilder().Build(ContainerBuildOptions.None);
            }

            return (ILifetimeScope)this._container;
        }

        protected override void RequestStartup(ILifetimeScope container, IPipelines pipelines, NancyContext context)
        {
            var stopW = Stopwatch.StartNew();

            var configuration = new StatelessAuthenticationConfiguration(nancyContext =>
            {
                var identityProvider = container.Resolve<IUserIdentityProvider>();
                return identityProvider.GetUserIdentity(nancyContext);
            });

            pipelines.BeforeRequest.AddItemToEndOfPipeline(StatelessAuthenticationHook.HandleRequest(configuration));
            pipelines.AfterRequest.AddItemToEndOfPipeline(x => AfterRequestHook.HandleResponse(x, stopW.ElapsedMilliseconds));
            pipelines.OnError.AddItemToEndOfPipeline((ctx, ex) => OnErrorHook.HandleError(context, ex, stopW.ElapsedMilliseconds));
        }

        protected override void ConfigureApplicationContainer(ILifetimeScope existingContainer)
        {
            this._logger.Debug("Register service configuration instance");

            var builder = new ContainerBuilder();
            builder.RegisterType<NancyUserIdentityProvider>().As<IUserIdentityProvider>().InstancePerLifetimeScope();
            builder.RegisterType<NancyTokenProvider>().As<ITokenProvider>().InstancePerLifetimeScope();

            this.RegisterCommonModules(builder);
            this.RegisterHostModules(builder);

            AutofacContainerWrapper.SetContainer((IContainer)existingContainer);
            builder.Update(existingContainer.ComponentRegistry);
        }

        private void RegisterCommonModules(ContainerBuilder builder)
        {
            builder.RegisterModule<UserManagementModule>();
            builder.RegisterModule<TokenModule>();
            builder.RegisterModule<DataAccessModule>();
        }

        private void RegisterHostModules(ContainerBuilder builder)
        {
            builder.RegisterModule<MapperModule>();
            builder.RegisterModule<ValidationModule>();
        }
    }
}
