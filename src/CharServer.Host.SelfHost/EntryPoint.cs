﻿using System;
using CharServer.Host.SelfHost.Owin;
using ChatServer.Contracts.Configuration;
using Microsoft.Owin.Hosting;
using NLog;

namespace CharServer.Host.SelfHost
{
    internal class EntryPoint
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        static void Main(string[] args)
        {
            try
            {
                LogManager.EnableLogging();

                var config = ServerConfiguration.Instance;

                Logger.Info(@"CharServer start at '{0}'", config.BaseHostUrl);

                using (WebApp.Start(config.BaseHostUrl, app => new OwinNancyStartup().Configuration(app, config)))
                {
                    Logger.Info("Running on '{0}'", config.BaseHostUrl);
                    Logger.Info("Press enter to exit");
                    Console.ReadLine();
                }
            }
            catch (Exception ex)
            {
                Logger.Info("An exception '{0}' occured on startup", ex.Message);
                Logger.Fatal(ex, "OWIN app failed to start");
                Console.ReadLine();
            }
        }
    }
}
