﻿using System;
using Autofac;
using Autofac.Builder;
using ChatServer.Contracts;

namespace CharServer.Host.SelfHost.Owin
{
    class ContainerStartup
    {
        public static IContainer PerformRegistrations(IServerConfiguration configuration)
        {
            if (configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }

            var builder = new ContainerBuilder();

            builder.RegisterInstance(configuration)
                .AsSelf()
                .As<IServerConfiguration>()
                .SingleInstance();

            return builder.Build(ContainerBuildOptions.None);
        }
    }
}
