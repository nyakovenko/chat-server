﻿using CharServer.Host.SelfHost.Bootstrap;
using ChatServer.Contracts;
using ChatServer.SignalR;
using ChatServer.SignalR.Authentication;
using ChatServer.SignalR.Container;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Nancy.Owin;
using Owin;

namespace CharServer.Host.SelfHost.Owin
{
    class OwinNancyStartup
    {
        public void Configuration(IAppBuilder app, IServerConfiguration configuration)
        {
            var container = ContainerStartup.PerformRegistrations(configuration);

            //var hubConfiguration = new HubConfiguration() { Resolver = new AutofacDependencyResolver(container) };
            var hubConfiguration = new HubConfiguration() { };
            hubConfiguration.Resolver.Resolve<IHubPipeline>().AddModule(new ErrorHandlingPipeLineModule());

            app.UseSignalRAuthMiddleware();

            app.MapSignalR("/signalr", hubConfiguration);

            app.UseNancy(new NancyOptions { Bootstrapper = new NancyHostBootstrapper(container) });
        }
    }
}
