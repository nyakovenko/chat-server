﻿using ChatServer.Contracts.Requests.Token;
using ChatServer.Contracts.Responses.Token;
using ChatServer.Contracts.Services;
using ChatServer.Host.Common.Constants.Services;
using ChatServer.NancyFx.Misc;
using Nancy;

namespace ChatServer.NancyFx.Module.Token
{
    public class TokenModule : NancyModule
    {
        public TokenModule(ITokenService tokenService)
            : base(TokenModuleConst.Service.ServiceRootPath)
        {
            Post[TokenModuleConst.UriTemplate.TokenUri, true] = async (ctx, ct) =>
            {
                return await this.HandleRequestAsync<TokenRequest, TokenResponse>(
                   async (request) => await tokenService.Authenticate(request));
            };
        }
    }
}
