﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace ChatServer.Common.Cryptography
{
    public static class PasswordGenerator
    {
        private static readonly Func<byte[], byte[], byte[]> HashFactory = (key, value) =>
        {
            using (var sha = new HMACSHA256(key)) { return sha.ComputeHash(value); }
        };

        public static void GeneratePassword(string key, uint saltSize,
            out string passwordHash,
            out string password,
            out string salt)
        {
            password = Guid.NewGuid().ToString().Replace("-", string.Empty);

            var bSalt = CreateSalt(saltSize);
            var bPassword = Encoding.UTF8.GetBytes(password);
            var bKey = Encoding.UTF8.GetBytes(key);

            var bSaltedPassword = Concatenate(bPassword, bSalt);
            var bHash = HashFactory(bKey, bSaltedPassword);

            passwordHash = Convert.ToBase64String(bHash);
            salt = Convert.ToBase64String(bSalt);
        }

        public static void ComputePassword(string key, string password, string salt,
            out string passwordHash)
        {
            var bSalt = Convert.FromBase64String(salt);
            var bPassword = Convert.FromBase64String(password);
            var bKey = Encoding.UTF8.GetBytes(key);

            var bSaltedPassword = Concatenate(bPassword, bSalt);
            var bHash = HashFactory(bKey, bSaltedPassword);

            passwordHash = Convert.ToBase64String(bHash);
        }

        public static void CreatePassword(string key, uint saltSize, string password,
           out string passwordHash,
           out string salt)
        {
            var bSalt = CreateSalt(saltSize);
            var bPassword = Convert.FromBase64String(password);
            var bKey = Encoding.UTF8.GetBytes(key);

            var bSaltedPassword = Concatenate(bPassword, bSalt);
            var bHash = HashFactory(bKey, bSaltedPassword);

            passwordHash = Convert.ToBase64String(bHash);
            salt = Convert.ToBase64String(bSalt);
        }

        public static byte[] CreateSalt(uint saltSize)
        {
            byte[] randomBytes = new byte[saltSize];
            using (var rnd = new RNGCryptoServiceProvider())
            {
                rnd.GetBytes(randomBytes);
                return randomBytes;
            }
        }

        private static byte[] Concatenate(byte[] blockA, byte[] blockB)
        {
            byte[] result = new byte[blockA.Length + blockB.Length];

            Buffer.BlockCopy(blockA, 0, result, 0, blockA.Length);
            Buffer.BlockCopy(blockB, 0, result, blockA.Length, blockB.Length);

            return result;
        }
    }
}
