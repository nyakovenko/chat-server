﻿using System;

namespace ChatServer.Common.Cryptography
{
    public static class ExpirationDateGenerator
    {
        private static readonly DateTime UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public static double GetExpirationTime(
            uint? daysAhead = null, uint? hoursAhead = null,
            uint? minutesAhead = null, uint? secondsAhead = null)
        {
            var time = DateTime.UtcNow;

            if (daysAhead.HasValue)
            {
                time = time.AddDays(daysAhead.Value);
            }

            if (hoursAhead.HasValue)
            {
                time = time.AddHours(hoursAhead.Value);
            }

            if (minutesAhead.HasValue)
            {
                time = time.AddMinutes(minutesAhead.Value);
            }

            if (secondsAhead.HasValue)
            {
                time = time.AddSeconds(secondsAhead.Value);
            }

            return GetExpirationTime(time);
        }

        public static double GetExpirationTime(DateTime time)
        {
            return Math.Round((time - UnixEpoch).TotalSeconds);
        }

        public static double CurrentTime
        {
            get
            {
                return GetExpirationTime(DateTime.UtcNow);
            }
        }
    }
}
