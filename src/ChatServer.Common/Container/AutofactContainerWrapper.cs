﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;

namespace ChatServer.Common.Container
{
    /// <summary>
    /// Wrapps autofac container to prevent direct dependency on autofac
    /// </summary>
    public class AutofacContainerWrapper
    {
        // thread safe single instance
        private static readonly Lazy<AutofacContainerWrapper> SingletonLazy =
            new Lazy<AutofacContainerWrapper>(() => new AutofacContainerWrapper());

        // native autofac container which should be warpped
        private IContainer _container;

        /// <summary>
        ///     Gets singleton object
        /// </summary>
        private static AutofacContainerWrapper Singleton
        {
            get { return SingletonLazy.Value; }
        }


        /// <summary>
        ///     Setting up container
        /// </summary>
        /// <param name="container">Autofac container</param>
        public static void SetContainer(IContainer container)
        {
            Singleton._container = container;
        }

        /// <summary>
        ///     Starting lifetime scope
        /// </summary>
        /// <returns>Container lifetime scope</returns>
        public static ILifetimeScope BeginScope()
        {
            return Singleton._container.BeginLifetimeScope(Guid.NewGuid());
        }

        /// <summary>
        ///     Resolving type
        /// </summary>
        /// <typeparam name="T">Type to resolve</typeparam>
        /// <returns>Resolved instance</returns>
        public static T ResolveStatic<T>()
        {
            return Singleton.Resolve<T>();
        }

        /// <summary>
        ///     Check if type is registerd in container
        /// </summary>
        /// <param name="type">Type to check</param>
        /// <returns>Registered or not</returns>
        public static bool IsRegistered(Type type)
        {
            return Singleton._container.IsRegistered(type);
        }

        /// <summary>
        ///     Resolving type
        /// </summary>
        /// <param name="type">Type to resolve</param>
        /// <returns>Resolved instance</returns>
        public static object ResolveStatic(Type type)
        {
            return Singleton.Resolve(type);
        }

        /// <summary>
        /// Check whether container was initialized or not
        /// </summary>
        /// <returns>True - already inited</returns>
        public static bool IsInited()
        {
            return null != Singleton._container;
        }

        /// <summary>
        ///     Resolving type
        /// </summary>
        /// <typeparam name="T">Type to resolve</typeparam>
        /// <returns>Resolved instance</returns>
        public T Resolve<T>()
        {
            return Singleton._container.Resolve<T>();
        }

        /// <summary>
        ///     Resolving type
        /// </summary>
        /// <param name="type">Type to resolve</param>
        /// <returns>Resolved instance</returns>
        public object Resolve(Type type)
        {
            return Singleton._container.Resolve(type);
        }

        /// <summary>
        ///     Resolving type by name
        /// </summary>
        /// <typeparam name="T">Type to resolve</typeparam>
        /// <param name="name">Type name shortcut</param>
        /// <returns>Resolved instance</returns>
        public T ResolveNamed<T>(string name)
        {
            return Singleton._container.ResolveNamed<T>(name);
        }

        /// <summary>
        ///     Resolving type with parameters
        /// </summary>
        /// <typeparam name="T">Type to resolve</typeparam>
        /// <param name="paramSet">Ctor parameters</param>
        /// <returns>Resolved instance</returns>
        public T ResolveWithParams<T>(IEnumerable<object> paramSet)
        {
            var autofacParams = paramSet.Select(x => new TypedParameter(x.GetType(), x));
            return Singleton._container.Resolve<T>(autofacParams);
        }
    }
}