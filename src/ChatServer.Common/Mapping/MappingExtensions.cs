﻿using AutoMapper;

namespace ChatServer.Common.Mapping
{
    public static class MappingExtensions
    {
        public static IMappingExpression IgnoreAllNonExisting(this IMappingExpression expression)
        {
            foreach (var property in expression.TypeMap.GetUnmappedPropertyNames())
            {
                expression.ForMember(property, opt => opt.Ignore());
            }
            return expression;
        }

        public static IMappingExpression<TSource, TDesctionation> IgnoreAllNonExisting<TSource, TDesctionation>(
            this IMappingExpression<TSource, TDesctionation> expression)
        {
            foreach (var property in expression.TypeMap.GetUnmappedPropertyNames())
            {
                expression.ForMember(property, opt => opt.Ignore());
            }
            return expression;
        }
    }
}
