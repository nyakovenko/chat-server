﻿using System.Collections.Generic;

namespace ChatServer.Common.Mapping.Engine
{
    public interface IMappingEngineWrapper
    {
        TTo Map<TTo>(object @from);
        IEnumerable<TTo> Map<TFrom, TTo>(IEnumerable<TFrom> collection);
        TTo Map<TFrom, TTo>(TFrom @from, TTo destination);
    }
}