﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;

namespace ChatServer.Common.Mapping.Engine
{
    public class MappingEngineWrapper : IMappingEngineWrapper
    {
        private readonly IMapper _engine;

        public MappingEngineWrapper(IMapper engine)
        {
            _engine = engine;
        }

        public TTo Map<TTo>(object @from)
        {
            return _engine.Map<TTo>(from);
        }

        public IEnumerable<TTo> Map<TFrom, TTo>(IEnumerable<TFrom> collection)
        {
            if (null == collection) return null;

            var resultSet = collection.Select(x => Map<TTo>(x));
            return resultSet;
        }

        public TTo Map<TFrom, TTo>(TFrom @from, TTo destination)
        {
            return _engine.Map(from, destination);
        }
    }
}
