﻿using System.Collections.Generic;

namespace ChatServer.Common.Mapping
{
    public interface IMapper<TFrom, TTo>
    {
        TFrom Map(TTo to);

        TTo Map(TFrom from);

        IEnumerable<TTo> Map(IEnumerable<TFrom> collection);

        TTo Map(TFrom from, TTo destination);
    }
}
