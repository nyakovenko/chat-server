﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;

namespace ChatServer.Common.Mapping
{
    public class MapperWrapper<TFrom, TTo> : IMapper<TFrom, TTo>
    {
        private readonly IMapper _engine;

        public MapperWrapper(IMapper engine)
        {
            _engine = engine;
        }

        public TFrom Map(TTo to)
        {
            return this._engine.Map<TFrom>(to);
        }

        public TTo Map(TFrom @from)
        {
            return _engine.Map<TTo>(from);
        }

        public IEnumerable<TTo> Map(IEnumerable<TFrom> collection)
        {
            if (null == collection)
            {
                return null;
            }

            var resultSet = collection.Select(Map);

            return resultSet;
        }

        public TTo Map(TFrom @from, TTo destination)
        {
            return _engine.Map(from, destination);
        }
    }
}
