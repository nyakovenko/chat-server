﻿using ChatServer.Contracts.Requests.User;
using ChatServer.Contracts.Responses.User;
using ChatServer.Contracts.Services;
using ChatServer.Host.Common.Constants.Services;
using ChatServer.NancyFx.Misc;
using Nancy;

namespace ChatServer.NancyFx.Module.UserManagement
{
    public class UserManagementModule : NancyModule
    {
        public UserManagementModule(IRegistrationService registrationService)
            : base(UserManagementModuleConst.Service.ServiceRootPath)
        {

            Post[UserManagementModuleConst.UriTemplate.RegisterUri, true] = async (ctx, ct) =>
            {
                return await this.HandleRequestAsync<RegisterUserRequest, RegisterUserResponse>(
                    async (request) => await registrationService.Register(request));
            };
        }
    }
}
