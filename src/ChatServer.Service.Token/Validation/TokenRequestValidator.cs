﻿using ChatServer.Contracts.Requests.Token;
using FluentValidation;

namespace ChatServer.Service.Token.Validation
{
    public class TokenRequestValidator : AbstractValidator<TokenRequest>
    {
        public TokenRequestValidator()
        {
            RuleFor(x => x.Username)
                .NotNull()
                .WithMessage("Username required");

            RuleFor(x => x.Password)
                .NotEmpty()
                .WithMessage("Password required");
        }
    }
}
