﻿using System.Threading.Tasks;
using ChatServer.Common.Cryptography;
using ChatServer.Contracts;
using ChatServer.Contracts.Models;
using ChatServer.Contracts.Requests.Token;
using ChatServer.Contracts.Responses.Token;
using ChatServer.Contracts.Services;
using ChatServer.Domain.Contracts;
using JWT;

namespace ChatServer.Service.Token
{
    public class TokenService : ITokenService
    {
        private readonly IUserRepository _userIdentityRepository;
        private readonly string _secretKey;
        private readonly uint _tokenExpTime;

        public TokenService(IUserRepository userIdentityRepository, IServerConfiguration serviceConfiguration)
        {
            this._userIdentityRepository = userIdentityRepository;
            this._secretKey = serviceConfiguration.Crypto.Secret;
            this._tokenExpTime = serviceConfiguration.Crypto.TokenExpirationTimeIsSeconds;
        }

        public async Task<TokenResponse> Authenticate(TokenRequest request)
        {
            var response = new TokenResponse();

            var user = await this._userIdentityRepository.FindByUserName(request.Username);

            if (user == null)
            {
                return (TokenResponse)response.ToUserNotFoundResponse();
            }

            string password;
            PasswordGenerator.ComputePassword(this._secretKey, request.Password, user.Salt, out password);

            if (user.Password == password)
            {
                var userIdentity = new IdentityToken()
                {
                    Claims = user.Claims.Split(' '),
                    City = user.City,
                    UserName = user.Username,
                    exp = ExpirationDateGenerator.GetExpirationTime(secondsAhead: this._tokenExpTime)
                };

                response.Message = "Token successfully generated";
                response.Details = "Put AccessToken to Autherization Header, when trying to access other API methods";
                response.AccessToken = JsonWebToken.Encode(userIdentity, this._secretKey, JwtHashAlgorithm.HS256);

                return response;
            }

            return response.ToInvalidPasswordResponse();
        }
    }
}
