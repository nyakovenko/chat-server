﻿using System;

namespace ChatServer.Domain.Contracts.Models
{
    public class Message : BaseEntity
    {
        public DateTime Date { get; set; }

        public string PublisherName { get; set; }

        public virtual User Publisher { get; set; }
    }
}
