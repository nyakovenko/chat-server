﻿using System.Collections.Generic;

namespace ChatServer.Domain.Contracts.Models
{
    public class User : BaseEntity
    {
        public User()
        {
            this.Messages = new HashSet<Message>();
            this.Claims = "User";
        }

        public string Username { get; set; }

        public string Password { get; set; }

        public string Salt { get; set; }

        public string Claims { get; set; }

        public string City { get; set; }

        public virtual ICollection<Message> Messages { get; set; }
    }
}
