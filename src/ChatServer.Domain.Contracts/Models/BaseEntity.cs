﻿namespace ChatServer.Domain.Contracts.Models
{
    public abstract class BaseEntity
    {
        public int Id { get; set; }
    }
}
