﻿using System.Threading.Tasks;
using ChatServer.Domain.Contracts.Models;

namespace ChatServer.Domain.Contracts
{
    public interface IUserRepository : IRepository<User>
    {
        Task<User> FindByUserName(string username);
    }
}
