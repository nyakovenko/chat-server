﻿using System.Threading;
using System.Threading.Tasks;

namespace ChatServer.Domain.Contracts
{
    public interface IUnitOfWork
    {
        int SaveChanges();
        Task<int> SaveChangesAsync();
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
