﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ChatServer.Domain.Contracts
{
    public interface IRepository<TEntity> where TEntity : class
    {
        Task<TEntity> Create(TEntity entity);

        Task<IEnumerable<TEntity>> Create(IList<TEntity> entity);

        Task<IEnumerable<TEntity>> GetAll();

        Task<IEnumerable<TEntity>> FindBy(Func<TEntity, bool> filter);

        Task<TEntity> Update(TEntity entity);

        Task Delete(TEntity entity);
    }
}
