﻿using System;
using System.Threading.Tasks;
using ChatServer.Contracts.Requests;
using ChatServer.Contracts.Responses;
using ChatServer.NancyFx.Misc.Exceptions;
using Nancy;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using Nancy.Validation;

namespace ChatServer.NancyFx.Misc
{
    public static class NancyModuleExtensions
    {
        public static async Task<Negotiator> HandleRequestAsync<TRequest, TResponse>(this NancyModule module, Func<TRequest, Task<TResponse>> actionFn)
            where TRequest : BaseRequest
            where TResponse : ResponseWithStatusCode
        {
            var request = module.BindAndValidateRequest<TRequest>();

            var response = await actionFn(request);

            return module.Negotiate.WithModel(response).WithStatusCode(response.StatusCode);
        }

        public static Negotiator HandleRequest<TRequest, TResponse>(this NancyModule module, Func<TRequest, TResponse> actionFn)
            where TRequest : BaseRequest
            where TResponse : ResponseWithStatusCode
        {
            var request = module.BindAndValidateRequest<TRequest>();

            var response = actionFn(request);

            return module.Negotiate.WithModel(response).WithStatusCode(response.StatusCode);
        }

        public static Task<T> BindAndValidateRequestAsync<T>(this NancyModule module) where T : BaseRequest
        {
            var request = BindAndValidateRequest<T>(module);
            return Task.FromResult(request);
        }

        public static T BindAndValidateRequest<T>(this NancyModule module) where T : BaseRequest
        {
            var request = module.Bind<T>();

            var validationResult = module.Validate(request);

            if (!validationResult.IsValid)
            {
                throw new RequestValidationException(validationResult);
            }

            return request;
        }
    }
}
