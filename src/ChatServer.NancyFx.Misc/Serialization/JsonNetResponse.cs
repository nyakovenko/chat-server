﻿using System;
using Nancy;
using Nancy.Responses;

namespace ChatServer.NancyFx.Misc
{
    public class JsonNetResponse
    {
        public static readonly Lazy<DefaultJsonSerializer> Serializer = new Lazy<DefaultJsonSerializer>();
        public static Response Create(object response, HttpStatusCode status = HttpStatusCode.OK)
        {
            var jsonResponse = new JsonResponse(response, Serializer.Value) as Response;
            jsonResponse.StatusCode = status;
            return jsonResponse;
        }
    }
}
