﻿using System;
using ChatServer.Host.Common;
using ChatServer.Host.Common.Constants;
using Nancy;
using NLog;

namespace ChatServer.NancyFx.Misc.Hooks
{
    public static class OnErrorHook
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public static dynamic HandleError(NancyContext ctx, Exception ex, long elapsedMs)
        {
            Logger.Debug(Consts.RequestProcessedMsg, ctx.Request.Path, ctx.Request.Method, elapsedMs);

            Logger.Error(ex);

            return JsonNetResponse.Create(new { Reason = "Internal Service Error" }, HttpStatusCode.InternalServerError);
        }
    }
}
