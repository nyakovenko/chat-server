﻿using ChatServer.Host.Common;
using ChatServer.Host.Common.Constants;
using Nancy;
using NLog;

namespace ChatServer.NancyFx.Misc.Hooks
{
    public static class AfterRequestHook
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public static void HandleResponse(NancyContext ctx, long elapsedMs)
        {
            if (!ctx.Response.Headers.ContainsKey(Consts.CacheControl))
                ctx.Response.Headers.Add(Consts.CacheControl, Consts.NoCache);

            Logger.Debug(Consts.RequestProcessedMsg, (object)ctx.Request.Path, (object)ctx.Request.Method, (object)elapsedMs);
        }
    }
}
