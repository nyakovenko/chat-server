﻿using System;
using ChatServer.Contracts.Responses;
using Nancy.Validation;

namespace ChatServer.NancyFx.Misc.Exceptions
{
    public class RequestValidationException : Exception
    {
        public object ValidationResult { get; private set; }

        public RequestValidationException(ModelValidationResult validationResult)
            : base("Request validation failed")
        {
            this.ValidationResult = new ResponseWithStatusCode().ToValidationResponse(validationResult);
        }
    }
}
