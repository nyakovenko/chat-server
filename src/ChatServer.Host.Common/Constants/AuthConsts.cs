﻿namespace ChatServer.Host.Common.Constants
{
    public static class AuthConsts
    {
        public const string AuthorizationHeaderKey = "Authorization";
        public const string AuthorizationTokenQueryStringKey = "token";
        public const string BearerToken = "Bearer";
    }
}
