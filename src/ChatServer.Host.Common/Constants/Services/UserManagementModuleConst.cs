﻿namespace ChatServer.Host.Common.Constants.Services
{
    public static class UserManagementModuleConst
    {
        public static class Service
        {
            public const string ServiceRootPath = Namespace.ChatServerPrefix + TargetServiceName;
            public const string TargetServiceName = "users";
        }

        public static class UriTemplate
        {
            public const string RegisterUri = "/register";
        }
    }
}
