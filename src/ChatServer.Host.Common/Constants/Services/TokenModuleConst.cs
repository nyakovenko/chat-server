﻿namespace ChatServer.Host.Common.Constants.Services
{
    public static class TokenModuleConst
    {
        public static class Service
        {
            public const string ServiceRootPath = Namespace.ChatServerPrefix + TargetServiceName;
            public const string TargetServiceName = "token";
        }

        public static class UriTemplate
        {
            public const string TokenUri = "/";
        }
    }
}
