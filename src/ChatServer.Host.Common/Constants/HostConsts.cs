﻿namespace ChatServer.Host.Common.Constants
{
    public static class HostConsts
    {
        public const string HostBaseUrlKey = "HostBaseUrl";
        public const string AssembliesForScanMask = "ChatServer.*.dll";
    }
}
