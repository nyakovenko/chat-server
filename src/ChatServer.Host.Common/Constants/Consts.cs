﻿namespace ChatServer.Host.Common.Constants
{
    public static class Consts
    {
        public const string CacheControl = "Cache-Control";
        public const string NoCache = "no-cache";
        public const string RequestProcessedMsg = "'{0}' [{1}] request was processed, within: {2} ms";
        public const string RequestStartMsg = "Service method '{0}' [{1}] call before proceed";
        public const string HttpHeadersMsg = "HTTP request Headers: {{{0}}}";
        public const string HttpBodyMsg = "HTTP request Body: '{0}'";
    }
}
