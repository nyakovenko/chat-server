﻿using Autofac;

namespace ChatServer.Host.Common.Modules
{
    public abstract class BaseModule : Module
    {
        protected abstract override void Load(ContainerBuilder builder);
    }
}
