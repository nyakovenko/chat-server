﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Autofac;
using AutoMapper;
using AutoMapper.Mappers;
using ChatServer.Common.Mapping;
using ChatServer.Common.Mapping.Engine;
using NLog;
using Module = Autofac.Module;

namespace ChatServer.Host.Common.Modules.Common
{
    public abstract class CommonMapperModule : Module
    {
        public abstract IList<string> AssembliesToScan { get; }

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        protected override void Load(ContainerBuilder builder)
        {
            var profiles = ScanAssembliesForMappingProfiles();

            var mapperConfiguration = new MapperConfiguration(configuration =>
            {
                foreach (var profile in profiles)
                {
                    configuration.AddProfile(profile);
                }
            });

            Logger.Debug("Assert that Mapper configuration is valid...");

            mapperConfiguration.AssertConfigurationIsValid();

            var mapper = mapperConfiguration.CreateMapper();

            builder.RegisterType<TypeMapFactory>().As<ITypeMapFactory>();
            builder.RegisterInstance(mapper).As<IMapper>().SingleInstance();
            builder.Register(c => MapperRegistry.Mappers);
            builder.RegisterInstance(mapperConfiguration)
                .AsSelf()
                .As<IConfigurationProvider>()
                .As<IConfiguration>()
                .As<IMapperConfiguration>()
                .SingleInstance();

            builder.RegisterGeneric(typeof(MapperWrapper<,>)).As(typeof(IMapper<,>));
            builder.RegisterType<MappingEngineWrapper>().As<IMappingEngineWrapper>().SingleInstance();
        }


        protected virtual IEnumerable<Profile> ScanAssembliesForMappingProfiles()
        {
            var asmTypes = new ConcurrentDictionary<Assembly, IList<Type>>();

            Parallel.ForEach(AssembliesToScan, file =>
            {
                var asm = Assembly.Load(AssemblyName.GetAssemblyName(file));
                var suitableTypes =
                    asm.GetTypes()
                        .Where(x => x.IsSubclassOf(typeof(Profile)) && null != x.GetConstructor(Type.EmptyTypes))
                        .ToList();
                asmTypes[asm] = suitableTypes;
            });

            var mapperProfiles = asmTypes.SelectMany(x => x.Value).ToArray();

            var mappersCnt = mapperProfiles.Length;

            Logger.Debug("{0} mapper profiles were found", mappersCnt);

            Logger.Debug("Attempting to initialize Mapper...");

            var profilesInst = new Profile[mappersCnt];
            Parallel.For(0, profilesInst.Length, (idx, state) =>
            {
                profilesInst[idx] = (Profile)Activator.CreateInstance(mapperProfiles[idx]);
            });

            return profilesInst;
        }
    }
}
