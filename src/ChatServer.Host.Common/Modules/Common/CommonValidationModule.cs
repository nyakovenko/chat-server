﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Autofac;
using FluentValidation;
using NLog;
using Module = Autofac.Module;

namespace ChatServer.Host.Common.Modules.Common
{
    public abstract class CommonValidationModule : Module
    {
        public abstract IList<string> AssembliesToScan { get; }

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        protected override void Load(ContainerBuilder builder)
        {
            var validators = ScanAssembliesForFluentValidators();

            Logger.Debug("Initializing Fluent Validation...");

            foreach (var validator in validators)
            {
                builder.RegisterType(validator)
                    .AsImplementedInterfaces()
                    .InstancePerLifetimeScope();
            }
        }

        protected virtual IEnumerable<Type> ScanAssembliesForFluentValidators()
        {
            var asmTypes = new ConcurrentDictionary<Assembly, IList<Type>>();

            Parallel.ForEach(AssembliesToScan, file =>
            {
                var asm = Assembly.Load(AssemblyName.GetAssemblyName(file));
                var suitableTypes =
                    asm.GetTypes()
                        .Where(
                            x => !x.IsGenericType &&
                                IsSubclassOfRawGeneric(typeof(AbstractValidator<>), x) &&
                                null != x.GetConstructor(Type.EmptyTypes))
                        .ToList();
                asmTypes[asm] = suitableTypes;
            });

            var validators = asmTypes.SelectMany(x => x.Value).ToArray();

            var validatorsCnt = validators.Length;

            Logger.Debug("{0} validators were found", validatorsCnt);

            return validators;
        }

        // taken from http://stackoverflow.com/a/457708
        static bool IsSubclassOfRawGeneric(Type generic, Type toCheck)
        {
            while (toCheck != null && toCheck != typeof(object))
            {
                var cur = toCheck.IsGenericType ? toCheck.GetGenericTypeDefinition() : toCheck;
                if (generic == cur)
                {
                    return true;
                }
                toCheck = toCheck.BaseType;
            }
            return false;
        }
    }
}
