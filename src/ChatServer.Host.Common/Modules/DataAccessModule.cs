﻿using Autofac;
using ChatServer.Domain.Contracts;
using ChatServer.Domain.EF;
using ChatServer.Domain.EF.Repositories;

namespace ChatServer.Host.Common.Modules
{
    public class DataAccessModule : BaseModule
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ChatServerContext>()
                .As<IChatServerContext>()
                .As<IUnitOfWork>()
                .InstancePerLifetimeScope();
            builder.RegisterType<UserRepository>().As<IUserRepository>().InstancePerLifetimeScope();
        }
    }
}
