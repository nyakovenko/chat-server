﻿using Autofac;
using ChatServer.Contracts.Services;
using ChatServer.Domain.Contracts;
using ChatServer.Domain.EF.Repositories;
using ChatServer.Service.Token;

namespace ChatServer.Host.Common.Modules
{
    public class TokenModule : BaseModule
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<TokenService>().As<ITokenService>().InstancePerLifetimeScope();
        }
    }
}
