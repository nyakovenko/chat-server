﻿using Autofac;
using ChatServer.Contracts.Services;
using ChatServer.Service.UserManagement;

namespace ChatServer.Host.Common.Modules
{
    public class UserManagementModule : BaseModule
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<RegistrationService>().As<IRegistrationService>().InstancePerLifetimeScope();
        }
    }
}
