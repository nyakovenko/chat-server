﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace ChatServer.SignalR
{
    public class ChatHubConnectionStorage
    {
        private readonly ConcurrentDictionary<string, HashSet<string>> _storage =
            new ConcurrentDictionary<string, HashSet<string>>();

        public int Count
        {
            get
            {
                return _storage.Count;
            }
        }

        public void Add(string key, string connectionId)
        {
            HashSet<string> connections;
            if (!_storage.TryGetValue(key, out connections))
            {
                connections = new HashSet<string>();
                _storage.TryAdd(key, connections);
            }

            lock (connections)
            {
                connections.Add(connectionId);
            }
        }

        public IEnumerable<string> GetConnections(string key)
        {
            HashSet<string> connections;
            if (_storage.TryGetValue(key, out connections))
            {
                return connections;
            }

            return Enumerable.Empty<string>();
        }

        public IEnumerable<string> GetClients()
        {
            return this._storage.Keys.ToList();
        }

        public void Remove(string key, string connectionId)
        {
            HashSet<string> connections;
            if (!_storage.TryGetValue(key, out connections))
            {
                return;
            }

            lock (connections)
            {
                connections.Remove(connectionId);

                if (connections.Count == 0)
                {
                    _storage.TryRemove(key, out connections);
                }
            }
        }
    }
}
