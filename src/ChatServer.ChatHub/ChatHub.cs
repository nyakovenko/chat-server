﻿using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;

namespace ChatServer.SignalR
{
    public class ChatHub : Hub
    {
        private static readonly ChatHubConnectionStorage _storage = new ChatHubConnectionStorage();

        public void SendMessage(string content, string timeMark)
        {
            var username = this.GetCurrentUserName();
            Clients.Others.broadcastMessage(content, timeMark, username);
        }

        public override Task OnConnected()
        {
            var client = this.GetCurrentUserName();

            Clients.Others.clientConnected(client);
            Clients.Caller.receiveListOfClients(_storage.GetClients());
           
            _storage.Add(client, this.Context.ConnectionId);
            
            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            var client = this.GetCurrentUserName();

            Clients.Others.clientDisconnected(client);

            _storage.Remove(client, this.Context.ConnectionId);

            return base.OnDisconnected(stopCalled);
        }

        private string GetCurrentUserName()
        {
            return this.Context.User != null ? this.Context.User.Identity.Name : "anonymous";
        }
    }
}
