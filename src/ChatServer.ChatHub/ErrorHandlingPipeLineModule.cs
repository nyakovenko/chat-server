﻿using Microsoft.AspNet.SignalR.Hubs;
using NLog;

namespace ChatServer.SignalR
{
    public class ErrorHandlingPipeLineModule : HubPipelineModule
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        protected override void OnIncomingError(ExceptionContext exceptionContext, IHubIncomingInvokerContext invokerContext)
        {
            Logger.Error("An exception occured during Hub invocation -" + exceptionContext.Error.Message);

            if (exceptionContext.Error.InnerException != null)
            {
                Logger.Error("Inner Exception " + exceptionContext.Error.InnerException.Message);
            }

            base.OnIncomingError(exceptionContext, invokerContext);
        }
    }
}
