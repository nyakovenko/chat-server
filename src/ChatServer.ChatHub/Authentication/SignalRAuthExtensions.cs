﻿using Owin;

namespace ChatServer.SignalR.Authentication
{
    public static class SignalRAuthExtensions
    {
        public static IAppBuilder UseSignalRAuthMiddleware(this IAppBuilder app)
        {
            app.Use<SignalRAuthMiddleware>();
            return app;
        }
    }
}