﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.Owin;

namespace ChatServer.SignalR.Authentication
{
    public class SignalRAuthMiddleware : OwinMiddleware
    {
        public SignalRAuthMiddleware(OwinMiddleware next)
            : base(next)
        {
        }

        public override async Task Invoke(IOwinContext context)
        {
            var token = SignalRUserIdentityProvider.GetUserIdentity(context);

            if (token != null)
            {
                var identity = new ClaimsIdentity("bearer token");
                identity.AddClaim(new Claim(ClaimTypes.Role, token.Claims.FirstOrDefault()));
                identity.AddClaim(new Claim(ClaimTypes.Name, token.UserName));
                identity.AddClaim(new Claim(ClaimTypes.StreetAddress, token.City));
                context.Request.User = new ClaimsPrincipal(identity);
            }

            await Next.Invoke(context);
        }
    }
}
