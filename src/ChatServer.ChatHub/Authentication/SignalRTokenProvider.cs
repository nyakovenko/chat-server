﻿using ChatServer.Host.Common.Constants;
using Microsoft.Owin;

namespace ChatServer.SignalR.Authentication
{
    public static class SignalRTokenProvider
    {
        public static string RequestToken(IOwinContext context)
        {
            if (context == null)
            {
                return null;
            }

            var tokenData = context.Request.Query.Get(AuthConsts.AuthorizationTokenQueryStringKey);

            return tokenData;
        }
    }
}
