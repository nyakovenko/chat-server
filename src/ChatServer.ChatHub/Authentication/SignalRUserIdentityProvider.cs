﻿using System;
using ChatServer.Common.Container;
using ChatServer.Contracts;
using ChatServer.Contracts.Models;
using JWT;
using Microsoft.Owin;
using NLog;

namespace ChatServer.SignalR.Authentication
{
    public static class SignalRUserIdentityProvider
    {
        private static readonly string Secret;
        private static readonly ILogger Logger = LogManager.GetCurrentClassLogger();

        static SignalRUserIdentityProvider()
        {
            var conf = AutofacContainerWrapper.ResolveStatic<IServerConfiguration>();
            Secret = conf.Crypto.Secret;
        }

        public static IdentityToken GetUserIdentity(IOwinContext ctx)
        {


            try
            {
                var token = SignalRTokenProvider.RequestToken(ctx);

                if (token == null)
                {
                    return null;
                }

                return JsonWebToken.DecodeToObject<IdentityToken>(token, Secret, verify: true);
            }
            catch (SignatureVerificationException ex)
            {
                return null;
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                return null;
            }
        }
    }
}
