using ChatServer.Contracts.Configuration;

namespace ChatServer.Contracts
{
    public interface IServerConfiguration
    {
        string DbConnectionString { get; set; }
        string BaseHostUrl { get; set; }

        CryptographyConfiguration Crypto { get; set; }
    }
}