﻿using System.Collections.Generic;

namespace ChatServer.Contracts.Models
{
    public class IdentityToken : IdentityWithExpirationMark
    {
        public string UserName { get; set; }

        public string City { get; set; }

        public IEnumerable<string> Claims { get; set; }
    }
}
