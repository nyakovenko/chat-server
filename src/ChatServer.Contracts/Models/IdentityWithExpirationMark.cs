﻿namespace ChatServer.Contracts.Models
{
    public abstract class IdentityWithExpirationMark
    {
        public object exp { get; set; }
    }
}
