﻿using System;
using System.Net;
using ChatServer.Contracts.Enums;

namespace ChatServer.Contracts.Responses.User
{
    public static class RegisterUserResponseExtensions
    {
        public static RegisterUserResponse ToUnableRegisterNewUserResponse(this RegisterUserResponse responseWithStatusCode, string username)
        {
            responseWithStatusCode.Message = "Unable to register new User";
            responseWithStatusCode.Details = string.Format("User with {0} username is already registered", username);
            responseWithStatusCode.State = RegistrationResultType.AlreadyRegistered;
            responseWithStatusCode.SetStatusCode(HttpStatusCode.Found);
            return responseWithStatusCode;
        }

        public static RegisterUserResponse ToInternalErrorResponse(this RegisterUserResponse responseWithStatusCode, Exception ex)
        {
            responseWithStatusCode.Message = "Internal error occured";
            responseWithStatusCode.Details = ex.Message;
            responseWithStatusCode.State = RegistrationResultType.Failed;
            responseWithStatusCode.SetStatusCode(HttpStatusCode.InternalServerError);
            return responseWithStatusCode;
        }

        public static RegisterUserResponse ToSuccessfulResponse(this RegisterUserResponse responseWithStatusCode, string username)
        {
            responseWithStatusCode.Message = "New user successfully registered";
            responseWithStatusCode.Details = string.Format("User {0} registration has been successful", username);
            responseWithStatusCode.State = RegistrationResultType.Succeed;
            responseWithStatusCode.SetStatusCode(HttpStatusCode.OK);
            return responseWithStatusCode;
        }
    }
}
