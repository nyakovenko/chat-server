﻿using ChatServer.Contracts.Enums;

namespace ChatServer.Contracts.Responses.User
{
    public class RegisterUserResponse : ResponseWithStatusCode
    {
        public RegistrationResultType State { get; set; }
    }
}
