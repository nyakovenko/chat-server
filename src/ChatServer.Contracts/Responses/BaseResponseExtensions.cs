﻿using System.Net;
using System.Text;

namespace ChatServer.Contracts.Responses
{
    public static class BaseResponseExtensions
    {
        public static ResponseWithStatusCode ToValidationResponse(this ResponseWithStatusCode responseWithStatusCode, dynamic result)
        {
            responseWithStatusCode.SetStatusCode(HttpStatusCode.BadRequest);
            responseWithStatusCode.Message = "Incorrect request format";

            var sb = new StringBuilder();
            foreach (var error in result.Errors)
            {
                sb.AppendLine("Property name: " + error.Key);
                foreach (var errorDetail in error.Value)
                {
                    sb.AppendLine("Error: " + errorDetail.ErrorMessage);
                }
            }

            responseWithStatusCode.Details = sb.ToString();
            return responseWithStatusCode;
        }
    }
}
