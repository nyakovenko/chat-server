﻿using System.Net;

namespace ChatServer.Contracts.Responses
{
    public class ResponseWithStatusCode : BaseResponse
    {
        public ResponseWithStatusCode()
        {
            this.SetStatusCode(HttpStatusCode.OK);
        }

        public string Message { get; set; }

        public string Details { get; set; }

        public int StatusCode { get; private set; }

        public void SetStatusCode(HttpStatusCode code)
        {
            this.StatusCode = (int)code;
        }
    }
}
