﻿using System.Net;

namespace ChatServer.Contracts.Responses.Token
{
    public static class TokenResponseExtensions
    {
        public static TokenResponse ToInvalidPasswordResponse(this TokenResponse responseWithStatusCode)
        {
            responseWithStatusCode.Message = "Verification failed";
            responseWithStatusCode.Details = "Provided password does not match with one on our db";
            responseWithStatusCode.SetStatusCode(HttpStatusCode.BadRequest);
            return responseWithStatusCode;
        }

        public static ResponseWithStatusCode ToUserNotFoundResponse(this ResponseWithStatusCode responseWithStatusCode)
        {
            responseWithStatusCode.Message = "User not found";
            responseWithStatusCode.Details = "User with provided id was not found";
            responseWithStatusCode.SetStatusCode(HttpStatusCode.NotFound);
            return responseWithStatusCode;
        }
    }
}
