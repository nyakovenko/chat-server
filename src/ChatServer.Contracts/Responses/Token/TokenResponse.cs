﻿namespace ChatServer.Contracts.Responses.Token
{
    public class TokenResponse : ResponseWithStatusCode
    {
        public string AccessToken { get; set; }
    }
}
