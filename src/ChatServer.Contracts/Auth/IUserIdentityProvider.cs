﻿using ChatServer.Contracts.Models;

namespace ChatServer.Contracts.Auth
{
    public interface IUserIdentityProvider
    {
        IdentityToken GetUserIdentity(object ctx);
    }
}
