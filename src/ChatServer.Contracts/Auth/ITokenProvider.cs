﻿namespace ChatServer.Contracts.Auth
{
    public interface ITokenProvider
    {
        string RequestToken(object ctx);
    }
}
