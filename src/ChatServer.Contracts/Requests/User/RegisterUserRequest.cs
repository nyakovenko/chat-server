﻿namespace ChatServer.Contracts.Requests.User
{
    public class RegisterUserRequest : BaseRequest
    {
        public string Username { get; set; }

        public string Password { get; set; }

        public string City { get; set; }
    }
}
