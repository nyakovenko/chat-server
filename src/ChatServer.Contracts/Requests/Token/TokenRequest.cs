﻿namespace ChatServer.Contracts.Requests.Token
{
    public class TokenRequest : BaseRequest
    {
        public string Username { get; set; }

        public string Password { get; set; }
    }
}
