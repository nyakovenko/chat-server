using System.Threading.Tasks;
using ChatServer.Contracts.Requests.User;
using ChatServer.Contracts.Responses.User;

namespace ChatServer.Contracts.Services
{
    public interface IRegistrationService
    {
        Task<RegisterUserResponse> Register(RegisterUserRequest request);
    }
}