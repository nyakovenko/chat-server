﻿using System.Threading.Tasks;
using ChatServer.Contracts.Requests.Token;
using ChatServer.Contracts.Responses.Token;

namespace ChatServer.Contracts.Services
{
    public interface ITokenService
    {
        Task<TokenResponse> Authenticate(TokenRequest request);
    }
}