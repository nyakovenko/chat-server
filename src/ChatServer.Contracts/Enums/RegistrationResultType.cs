﻿namespace ChatServer.Contracts.Enums
{
    public enum RegistrationResultType
    {
        Succeed,
        AlreadyRegistered,
        Failed
    }
}
