﻿using System;
using System.Net;

namespace ChatServer.Contracts.Exceptions
{
    public class InvalidAuthTokenException : Exception
    {
        public HttpStatusCode Code { get; private set; }
        public InvalidAuthTokenException(string message, HttpStatusCode code = HttpStatusCode.InternalServerError)
            : base(message)
        {
            this.Code = code;
        }
    }
}
