﻿using System.Configuration;

namespace ChatServer.Contracts.Configuration
{
    public class CryptographyConfiguration : ConfigurationElement
    {
        [ConfigurationProperty("Secret", IsKey = false, IsRequired = true)]
        public string Secret
        {
            get { return (string)base["Secret"]; }
            set { base["Secret"] = value; }
        }

        [ConfigurationProperty("SaltLength", IsKey = false, IsRequired = true)]
        public uint SaltLength
        {
            get { return (uint)base["SaltLength"]; }
            set { base["SaltLength"] = value; }
        }

        [ConfigurationProperty("TokenExpirationTimeIsSeconds", IsKey = false, IsRequired = true)]
        public uint TokenExpirationTimeIsSeconds
        {
            get { return (uint)base["TokenExpirationTimeIsSeconds"]; }
            set { base["TokenExpirationTimeIsSeconds"] = value; }
        }
    }
}
