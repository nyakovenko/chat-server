﻿using System;
using System.Configuration;

namespace ChatServer.Contracts.Configuration
{
    public class ServerConfiguration : ConfigurationSection, IServerConfiguration
    {
        private static readonly Lazy<ServerConfiguration> Configuration = new Lazy<ServerConfiguration>(() =>
            (ServerConfiguration) ConfigurationManager.GetSection("ChatServer.Configuration"));

        public static ServerConfiguration Instance
        {
            get { return Configuration.Value; }
        }

        [ConfigurationProperty("DbConnectionString", IsRequired = true)]
        public string DbConnectionString
        {
            get { return (string)base["DbConnectionString"]; }
            set { base["DbConnectionString"] = value; }
        }

        [ConfigurationProperty("BaseHostUrl", IsRequired = true)]
        public string BaseHostUrl
        {
            get { return (string)base["BaseHostUrl"]; }
            set { base["BaseHostUrl"] = value; }
        }

        [ConfigurationProperty("Crypto", IsRequired = true)]
        public CryptographyConfiguration Crypto
        {
            get { return (CryptographyConfiguration)base["Crypto"]; }
            set { base["Crypto"] = value; }
        }
    }
}
